# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# José Vieira <jvieira33@sapo.pt>, 2023
# Hugo Carvalho <hugokarvalho@hotmail.com>, 2023
# Paulo C., 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-19 16:07+0200\n"
"PO-Revision-Date: 2023-02-19 14:12+0000\n"
"Last-Translator: Paulo C., 2023\n"
"Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: icewm-quick-personal-menu-manager:13
msgid "You are running an IceWM desktop"
msgstr "Está a executar um ambiente de trabalho IceWM"

#: icewm-quick-personal-menu-manager:15 icewm-quick-personal-menu-manager:119
msgid "Warning"
msgstr "Aviso"

#: icewm-quick-personal-menu-manager:15
msgid "This script is meant to be run only in an IceWM desktop"
msgstr ""
"Este script destina-se a ser executado apenas num ambiente de trabalho IceWM"

#: icewm-quick-personal-menu-manager:77 icewm-quick-personal-menu-manager:85
#: icewm-quick-personal-menu-manager:101 icewm-quick-personal-menu-manager:133
#: icewm-quick-personal-menu-manager:153 icewm-quick-personal-menu-manager:218
#: icewm-quick-personal-menu-manager:327
msgid "Personal Menu Ultra Fast Manager"
msgstr "Gestor Ultra Rápido do Menu Pessoal"

#: icewm-quick-personal-menu-manager:77
msgid "Help::TXT"
msgstr "Ajuda::TXT"

#: icewm-quick-personal-menu-manager:77
msgid ""
"What is this?\\nThis utility adds and removes application icons from IceWm's"
" 'personal' list.\\nApplication icons are created from an application's "
".desktop file.\\nWhat are .desktop files?\\nUsually a .desktop file is "
"created during an application's installation process to allow the system "
"easy access to relevant information, such as the app's full name, commands "
"to be executed, icon to be used, where it should be placed in the OS menu, "
"etc.\\n.\\n Buttons:\\n 'ADD ICON' - select, from the list, application you "
"want to add to your 'personal' list and it instantly shows up in the menu or"
" submenu.\\nIf, for some reason, the correct icon for your application is "
"not found, a 'gears' icon will be used, so that you can still click it to "
"access the application.\\nYou can click the 'Advanced' button to manually "
"edit the relevant entry and change the application's icon.\\n'UNDO LAST "
"STEP' - every time an icon is added or removed from the toolbar, A backup "
"file is created. If you click this button, a restore is performed from that "
"backup file, without any confirmation.\\n'REMOVE ICON' - this shows a list "
"of all applications that have icons on your 'personal' list. Double left "
"click any application to remove its icon from the list\\n'ADVANCED' - allows"
" for editing the text configuration file that stores all of the entries in "
"your  'personal' list. Manually editing this file allows users to rearrange "
"the order of the icons and delete or add any icon. A brief explanation about"
" the inner workings of the text configuration file is displayed before the "
"file is opened for edition.\\n Warnings: only manually edit a configuration "
"file if you are sure of what you are doing! Always make a back up copy "
"before editing a configuration file!"
msgstr ""
"O que é isto?\\nEste utilitário adiciona e remove ícones de aplicações à sua"
" lista 'pessoal' no IceWM.\\nÍcones de aplicações são criados a partir do "
"respetivo ficheiro .desktop.\\nO que são ficheiros .desktop??\\nNormalmente,"
" um ficheiro .desktop é criado durante a instalação de uma aplicação, para "
"permitir que o sistema aceda a informação relevante sobre ela, como o nome "
"da aplicação, comandos necessários de executar, ícone a utilizar, a sua "
"categoria no menu, etc..\\n.\\n Botões:\\n 'ADICIONAR ÍCONE' - selecione, da"
" lista, a aplicação que deseja adicionar à sua lista 'pessoal' e ela surgirá"
" instantaneamente no menu ou submenu.\\nSe, por algum motivo, o ícone "
"correto da aplicação não for encontrado, um ícone de 'engrenagens' será "
"utilizado, de modo a poder clicar nele para aceder à aplicação.\\nPoderá "
"clicar no botão 'Conf. Avançada' para editar manualmente a entrada relevante"
" e alterar o ícone da aplicação.\\n'DESFAZER AÇÃO' - sempre que adiciona ou "
"remove um ícone, uma cópia de segurança é criada. Se clicar neste botão, é "
"feito um restauro a partir dessa cópia, sem qualquer confirmação.\\n'REMOVER"
" ÍCONE' - exibe todas as aplicações que estão na sua lista 'pessoal'. Faça "
"um duplo clique esquerdo sobre a aplicação a remover da lista\\n'Conf. "
"Avançada' - permite editar o ficheiro de texto que contém todas as entradas "
"da sua lista 'pessoal'. Ao editar manualmente este ficheiro, poderá "
"reordenar os ícones e remover ou adicionar qualquer ícone. Uma explicação "
"breve do funcionamento interno do ficheiro de texto com a configuração é "
"exibido antes de o abrir para edição.\\n AVISOS: só edite manualmente um "
"ficheiro de configuração se souber o que está a fazer! Faça sempre cópias de"
" segurança, antes de editar um ficheiro de configuração!"

#: icewm-quick-personal-menu-manager:85
msgid "Warning::TXT"
msgstr "Aviso::TXT"

#: icewm-quick-personal-menu-manager:85
msgid ""
"If you click to continue, the 'personal' configuration file will be opened for manual edition.\\n\n"
"How-to:\\nEach icon is identified by a line starting with 'prog' followed by the application name, icon and the application executable file.\\n Move, edit or delete the entire line referring to each entry.\\nNote: Lines starting with # are comments only and will be ignored.\\nThere can be empty lines.\\nAny changes appear instantly on the menu.\\nYou can undo the last change from UNDO LAST STEP button."
msgstr ""
"Se clicar para prosseguir, o ficheiro de entradas 'Pessoal' será aberto para edição manual.\\n\n"
"Instruções:\\n Cada ícone é identificado por uma linha que inicia com 'prog' seguido pelo \"nome\", \"ícone\" e ficheiro executável da aplicação.\\n Pode mover, alterar ou apagar toda a linha referente a cada aplicação.\\n Nota: As linhas iniciadas por # são apenas comentários e serão ignoradas.\\n Podem existir linhas em branco.\\n Qualquer mudança aparecerá instantaneamente no menu, após ser gravada.\\n Pode desfazer a última alteração a partir do botão DESFAZER AÇÃO."

#: icewm-quick-personal-menu-manager:101
msgid "Double click any Application to remove its icon:"
msgstr "Faça duplo clique numa aplicação para remover o seu ícone:"

#: icewm-quick-personal-menu-manager:101
msgid "Remove"
msgstr "Remover"

#: icewm-quick-personal-menu-manager:112
msgid "file has something"
msgstr "o ficheiro tem conteúdo"

#: icewm-quick-personal-menu-manager:117
msgid "file is empty"
msgstr "o ficheiro está vazio"

#: icewm-quick-personal-menu-manager:119
msgid "No changes were made!\\nTIP: you can always try the Advanced buttton."
msgstr ""
"Não foram feitas alterações!\\nDICA: tentar com o botão 'Conf AVANÇADA'."

#: icewm-quick-personal-menu-manager:133
msgid "Double click any Application to move its icon:"
msgstr "Faça duplo clique numa aplicação para mover o seu ícone:"

#: icewm-quick-personal-menu-manager:133
msgid "Move"
msgstr "Mover"

#: icewm-quick-personal-menu-manager:143
msgid "nothing was selected"
msgstr "não foi selecionado nada"

#: icewm-quick-personal-menu-manager:153
#, sh-format
msgid "Choose what do to with $EXEC icon"
msgstr "Escolha o que fazer com o ícone $EXEC"

#: icewm-quick-personal-menu-manager:218
msgid "Add selected app's icon"
msgstr "Adicionar ícone selecionado"

#: icewm-quick-personal-menu-manager:331
msgid "HELP!help:FBTN"
msgstr "Ajuda!help:FBTN"

#: icewm-quick-personal-menu-manager:332
msgid "ADVANCED!help-hint:FBTN"
msgstr "Conf. Avançada!help-hint:FBTN"

#: icewm-quick-personal-menu-manager:333
msgid "ADD ICON!add:FBTN"
msgstr "ADICIONAR ÍCONE!add:FBTN"

#: icewm-quick-personal-menu-manager:334
msgid "REMOVE ICON!remove:FBTN"
msgstr "REMOVER ÍCONE!remove:FBTN"

#: icewm-quick-personal-menu-manager:335
msgid "UNDO LAST STEP!undo:FBTN"
msgstr "DESFAZER AÇÃO!undo:FBTN"
