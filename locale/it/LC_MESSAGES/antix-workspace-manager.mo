��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     V     ^     g  T   s  O   �               8  *   =  B   h  �   �     O  =   m  9   �  8   �  4                                                       	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023
Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Applica Indietro Cambia nomi Clicca su Applica quando i nomi desiderati degli spazi di lavoro sono stati immessi. Clicca Fatto quando il numero desiderato di spazi di lavoro è stato impostato. Fatto Digita i nomi desiderati: Esci Imposta il numero desiderato di scrivanie: Per cambiare i nomi degli spazi di lavoro clicca su Modifica nomi. Usa la rotellina del mouse o clicca sulle frecce. Puoi anche inserire un numero tramite la tastiera seguito dal tasto Invio. Il cambiamento avviene immediatamente. Numero dello spazio di lavoro scambiatore di conteggio dello spazio di lavoro aWCS di antiX modificatore di nome dello spazio di lavoro aWCS di antiX scambiatore di conteggio dello spazio di lavoro di antiX modificatore di nome dello spazio di lavoro di antiX 