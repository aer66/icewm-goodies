��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  �  �     F     N     W  H   c  J   �  
   �            3   $  C   X  �   �     !  4   >  /   s  /   �  *   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Aplicar Anterior Mudar Nomes Clique em Aplicar quando tiver inserido os nomes das áreas de trabalho. Clique em Concluído quando tiver o número desejado de áreas de trabalho Concluído Insira os nomes pretendidos: Sair Escolha o número pretendido de áreas de trabalho: Para alterar os nomes das áreas de trabalho, clique em Mudar Nomes Use a roda do rato ou clique nas setas. Pode também inserir um número usando o teclado e a tecla enter. A alteração é imediata. Número da área de trabalho Alterar contagem de áreas de trabalho do antiX aWCS Alterar nome da área de trabalho do antiX aWCS Alterar contagem de áreas de trabalho do antiX Alterar nome da área de trabalho do antiX 