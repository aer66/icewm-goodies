��          t      �                 (  -   D     r     �     �  	     ^     "   x     �  �  �     i     }  0   �  o   �     9     B     V  Y   ^  "   �     �                  
                                      	    Add a (manual) command Add application from a list Enter command to be added to the startup file Enter command you want to run at antiX's startup. Note: an ampersand/& will automatically be appended to the end of the command Remove Remove an application Startup ( The line was added to $startupfile. It will start automatically the next time you start  antiX \n $add_remove_text $startupfile): antiX-startup GUI Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-15 17:18+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://app.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Dodaj ukaz (ročno) Dodaj aplikacijo s seznama Vnesite ukaz, ki naj se doda v zagonsko datoteko Vnesite ukaz, ki naj se izvede ob zagonu antiX. Opomba: ukazu bo na konec samodejno dodan znak za vezaj "et"/&. Odstrani Odstrani aplikacijo Zagon ( vrstica je bila dodana v $startupfile. Samodejno se bo zagnala ob naslednjem zagonu antiX \n $add_remove_text $startupfile): atniX-zagon grafični vmesnik 