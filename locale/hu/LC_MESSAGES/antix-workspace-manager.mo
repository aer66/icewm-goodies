��          �      ,      �     �     �     �  ?   �  5   �               5  #   ;  -   _  �   �       #   %  !   I     k     �  f  �               !  P   9  P   �     �     �  	   �  1     N   :  �   �     -  )   B  )   l  $   �  $   �                                                    	   
                       Apply Back Change Names Click Apply when desired names of workspaces have been entered. Click Done when desired number of workspaces was set. Done Enter the desired names: Leave Set the desired number of desktops: To change Workspace names click Change Names. Use mouse wheel or click on arrows. You can also enter a number by keyboard followed by enter key. The change takes place immediately. Workspace Number aWCS antiX workspace count switcher aWCS antiX workspace name changer antiX workspace count switcher antiX workspace name changer Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:48+0000
Last-Translator: Feri, 2023
Language-Team: Hungarian (https://www.transifex.com/anticapitalista/teams/10162/hu/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: hu
Plural-Forms: nplurals=2; plural=(n != 1);
 Alkalmazás Vissza Nevek megváltoztatása Nyomja meg az Alkalmazás gombot a kívánt munkaterület nevek beírása után. Kattintson a Kész gombra a kívánt munkaterületek számának megadása után. Kész Adja meg a kívánt neveket: Elhagyás Adja meg a használni kívánt asztalok számát: A munkaterületek nevének megváltoztatásához a Nevek megváltoztatására. Használja az egérgörgőt vagy a nyilakat. A billentyűzeten is megadhat egy számot, majd nyomja meg az Enter billentyűt. A változtatás azonnal életbe lép. Munkaterület száma aWCS antiX munkaterület szám választó aWCS antiX munkaterület név szerkesztő antiX munkaterület szám választó antiX munkaterület név szerkesztő 