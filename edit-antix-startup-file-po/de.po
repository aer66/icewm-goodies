# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Tony Ivanov <duskull88@fastmail.fm>, 2023
# Der Dings, 2023
# aer, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-05-16 13:19+0300\n"
"PO-Revision-Date: 2023-05-15 17:18+0000\n"
"Last-Translator: aer, 2023\n"
"Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: edit_antix_startup_file:10
msgid "antiX-startup GUI"
msgstr "antiX-startup-Einstellungen"

#: edit_antix_startup_file:11
msgid "Startup ("
msgstr "Startup ("

#: edit_antix_startup_file:12
msgid "Add application from a list"
msgstr "Eine Anwendung von einer Liste hinzufügen"

#: edit_antix_startup_file:13
msgid "Add a (manual) command"
msgstr "Einen (manuellen) Befehl hinzufügen"

#: edit_antix_startup_file:14
msgid "Remove an application"
msgstr "Eine Anwendung entfernen"

#: edit_antix_startup_file:15
#, sh-format
msgid ""
"The line was added to $startupfile. It will start automatically the next "
"time you start  antiX"
msgstr ""
"Die Zeile wurde zu $startupfile hinzugefügt. Sie wird beim nächsten Start "
"von antiX automatisch ausgeführt"

#: edit_antix_startup_file:16
msgid "Enter command to be added to the startup file"
msgstr "Befehl eingeben, der zur Startdatei hinzugefügt werden soll"

#: edit_antix_startup_file:17
msgid ""
"Enter command you want to run at antiX's startup. Note: an ampersand/& will "
"automatically be appended to the end of the command"
msgstr ""
"Befehl eingeben, der beim Start von antiX ausführt werden soll. Hinweis: Ein"
" & wird automatisch an das Ende des Befehls angehängt"

#: edit_antix_startup_file:19
msgid "Remove"
msgstr "Entfernen"

#: edit_antix_startup_file:22
#, sh-format
msgid "\\n $add_remove_text $startupfile):"
msgstr "\\n $add_remove_text $startupfile):"
